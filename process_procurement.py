# This utility provides interfaces to four separate categories of data, though
# we initially -- and primarily -- focus on the first three:
#
# 1. Proactive Disclosure of all Federal Contracts above 10k CAD. The dataset
#    is described at
#
#     https://open.canada.ca/data/en/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b
#
#    and the dataset itself at
#
#     https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/fac950c0-00d5-4ec1-a4d3-9cbebf98a305/download/contracts.csv
#
#    and its 'legacy' predecessor at
#
#     https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/7f9b18ca-f627-4852-93d5-69adeb9437d6/download/load-contracts-2020-10-01.csv
#
#    For details on the data fields, see Appendix A of
#     https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=14676
#
# 2. A history of all contracts awarded by Public Works and Government Services
#    Canada (PWGSC) since 2009. The datasets are described at
#
#     https://open.canada.ca/data/en/dataset/53753f06-8b28-42d7-89f7-04cd014323b0#wb-auto-6
#
#    and the complete file can be downloaded from
#
#     https://buyandsell.gc.ca/cds/public/contracts/tpsgc-pwgsc_co-ch_tous-all.csv
#
# 3. All award notices published to buyandsell.gc.ca. Each such award notice
#    announces a contract being award as a result of a tender notice.
#
#    Said dataset is described at
#
#     https://open.canada.ca/data/en/dataset/ffd38960-1853-4c19-ba26-e50bea2cb2d5
#
#    and directly available at
#
#      https://buyandsell.gc.ca/procurement-data/csv/award/all
#
#    As we demonstrate for Babel Street, Inc. and Dataminr, Inc., this category
#    contains a small percentage of our contracts of interest. The covered
#    contracts are restricted to those awarded by Public Works and Government
#    Services Canada (PWGSC) and typically only cover the first modification of
#    any award series. Thus, while the features are different, the covered
#    contracts are roughly a subset of those from Category (3).
#
#    We export this dataset as ca_buyandsell_award_notice.csv.
#
# The coverage of awards to Babel Street, Inc. and Dataminr, Inc. by these three
# datasets is instructive.
#
#
#                Contracts Involving "Babel Street, Inc."
#   ======================================================================
#                                             Included in Dataset Category
#      Identifier   Modification  Start Date        (1)  (2)  (3)
#   -------------   ------------  ----------        ---  ---  ---
#    M7594-184225            001  2020-11-25         -    X    -
#    "          "            000  2020-09-02         -    X    X
#      2BSCS11898                 2020-03-27         X    -    -
#         7414185                 2019-11-27         X    -    -
#   OSINT9063-001                 2019-03-15         X    -    -
#      4741983638                 2018-03-01         X    -    -
#         7378892                 2017-10-10         X    -    -
#      M500064818                 2016-12-28         X    -    -
#
#
# Complete coverage of these awards requires categories (1) and (2), and, as
# suggested above, category (3) roughly covers a subset of category (2)
# (albeit with different features).
#
#
#                Contracts Involving "Dataminr, Inc."
#  =======================================================================
#                                             Included in Dataset Category
#     Identifier   Modification  Start Date         (1)  (2)  (3)
#  -------------   ------------  ----------         ---  ---  ---
#   08324-160630            005  2020-05-25          -    X    -
#   "          "            004  2019-08-14          -    X    -
#   "          "            003  2018-08-03          -    X    -
#   "          "            002  2018-08-02          -    X    -
#   "          "            001  2018-01-04          -    X    X
#   "          "            000  2017-08-31          -    X    -
#       216-0630   2020-2021-Q2  2017-09-01          X    -    -
#       "      "   2020-2021-Q1  "        "          X    -    -
#       "      "   2019-2020-Q2  "        "          X    -    -
#       "      "   2018-2019-Q2  "        "          X    -    -
#       "      "   2017-2018-Q2  "        "          X    -    -
#
#
# We now return to the fourth and final category of data:
#
# 4. Tender notices document the initial call for bids and are therefore
#    upstream of award notices (which are the transition from tender to
#    contract) and contract histories. They are made available at:
#
#     https://open.canada.ca/data/en/dataset/ffd38960-1853-4c19-ba26-e50bea2cb2d5
#
#    but are split into four categories:
#
#    a) Construction,
#    b) Goods,
#    c) Services, and
#    d) Services Related to Goods.
#
# We do not yet make use of tender notices -- partially because the CSV files
# do not properly quote-escape strings containing commas.
#
import csv
import json
import numpy as np
import pandas as pd


# Proactive disclosure contract records -- category (1) -- dateset URL.
PROACTIVE_DISCLOSURE_URL = 'https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/fac950c0-00d5-4ec1-a4d3-9cbebf98a305/download/contracts.csv'
PROACTIVE_DISCLOSURE_LEGACY_URL = 'https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/7f9b18ca-f627-4852-93d5-69adeb9437d6/download/load-contracts-2020-10-01.csv'

# PWGSC Contract history -- category (2) -- dataset URLs.
PWSGC_CONTRACTS_URL = 'https://buyandsell.gc.ca/cds/public/contracts/tpsgc-pwgsc_co-ch_tous-all.csv'
PWSGC_CONTRACTS_RECENT_URL = 'https://buyandsell.gc.ca/cds/public/contracts/tpsgc-pwgsc_co-ch_EF-FY-20-21.csv'

# Award notice -- category (3) -- dataset URL.
AWARDS_URL = 'https://buyandsell.gc.ca/procurement-data/csv/award/all'
AWARDS_FRENCH_TAG = 'Français'

# Tender notice -- category (4) -- dataset URLs.
TENDER_CONSTRUCTION_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/construction'
TENDER_GOODS_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/goods'
TENDER_SERVICES_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/services'
TENDER_SERVICES_RELATED_TO_GOODS_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/services-related-to-goods'


def export_for_postgres(df, output_filename, unique_columns):
  # We must use a tab separator and avoid newlines or MySQL will truncate
  # all text inputs to 255 characters. This causes JSON parsing issues for
  # some of Google's "lobbyist_positions" fields.

  df = df.replace(r'\\r\\n', '; ', regex=True)
  df = df.replace(r'\\r', '; ', regex=True)
  df = df.replace(r'\\n', '; ', regex=True)

  df = df.replace(r'\r\n', '; ', regex=True)
  df = df.replace(r'\r', '; ', regex=True)
  df = df.replace(r'\n', '; ', regex=True)

  df = df.replace(r'\\', r'\\\\', regex=True)

  # Replace all sequences of the form '\...;' with ';' since PostgreSQL
  # refuses to parse JSON otherwise.
  df = df.replace(r'\\+;', r';', regex=True)

  # Remove redundant rows
  duplicated = df[unique_columns].duplicated(keep='first')
  num_dropped = sum(duplicated)
  print('Dropping {} redundant rows.'.format(num_dropped))
  df = df.drop(df[duplicated].index)

  df.to_csv(output_filename,
            index=False,
            escapechar='\\',
            quotechar='"',
            doublequote=False,
            sep='\t',
            quoting=csv.QUOTE_ALL)


def process_proactive_disclosures(
    csv_filename='ca_proactive_disclosure_filings_orig.csv',
    postgres_csv_filename='ca_proactive_disclosure_filing.csv',
    unique_vendors_filename='ca_proactive_disclosure_unique_vendors.json'):
    # Read in the CSV as a DataFrame
    df = pd.read_csv(PROACTIVE_DISCLOSURE_URL)

    # Drop the redundant French descriptions since we display in English.
    df = df.drop(columns=[
        'description_fr', 'comments_fr', 'additional_comments_fr'])
    df = df.rename(columns={
        'description_en': 'description',
        'comments_en': 'comments',
        'additional_comments_en': 'additional_comments'
    })

    # Drop several fields which we do not yet use.
    df = df.drop(columns=[
        'economic_object_code', 'land_claims',
        'commodity_type', 'commodity_code',
        'aboriginal_business', 'aboriginal_business_incidental',
        'potential_commercial_exploitation', 'former_public_servant',
        'ministers_office', 'article_6_exceptions', 'socioeconomic_indicator'])

    # Convert relevant columns to datetime.
    date_columns = ['contract_date', 'contract_period_start', 'delivery_date']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(
            df[date_column], format='%Y-%m-%d', errors='coerce')

    df['number_of_bids'] = df['number_of_bids'].round().astype('Int32')
    df['award_criteria'] = df['award_criteria'].round().astype('Int32')

    df.to_csv(csv_filename, index=False)

    unique_columns = [
        'reference_number',
        'procurement_id',
        'vendor_name',
        'contract_date',
        'buyer_name',
        'owner_org'
    ]
    export_for_postgres(df, postgres_csv_filename, unique_columns)

    unique_vendors = list(df['vendor_name'].fillna(value='').unique())
    unique_vendors_norm = []
    for vendor in unique_vendors:
        vendor = vendor.lower().strip()
        unique_vendors_norm.append(vendor)
    unique_vendors_norm = sorted(list(set(unique_vendors_norm)))
    with open(unique_vendors_filename, 'w') as outfile:
        json.dump(unique_vendors_norm, outfile, indent=1)

def process_pwsgc_contracts(
    csv_filename='ca_pwsgc_contract_filings_orig.csv',
    postgres_csv_filename='ca_pwsgc_contract_filing.csv',
    unique_suppliers_filename='ca_pwsgc_contract_unique_suppliers.json'):
    # Read in the CSV as a DataFrame
    df = pd.read_csv(PWSGC_CONTRACTS_URL, engine='c')

    # Drop the redundant French descriptions since we display in English.
    df = df.drop(columns=[
        'gsin-description_fr',
        'competitive-tender_fr',
        'limited-tender-reason-description_fr',
        'solicitation-procedure-description_fr',
        'trade-agreement-description_fr',
        'organization-employee-count_fr',
        'end-user-entity_fr',
        'contracting-entity-office-name_fr',
        'procurement-entity-name_fr',
        'country-description_fr'
    ])
    df = df.rename(columns={
        'gsin-description_en': 'gsin-description',
        'competitive-tender_en': 'competitive-tender',
        'limited-tender-reason-description_en': 'limited-tender-reason-description',
        'solicitation-procedure-description_en': 'solicitation-procedure-description',
        'trade-agreement-description_en': 'trade-agreement-description',
        'organization-employee-count_en': 'organization-employee-count',
        'end-user-entity_en': 'end-user-entity',
        'contracting-entity-office-name_en': 'contracting-entity-office-name',
        'procurement-entity-name_en': 'procurement-entity-name',
        'country-description_en': 'country-description'
    })

    # Convert relevant columns to datetime.
    date_columns = ['award-date', 'expiry-date', 'date-file-published']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(
            df[date_column], infer_datetime_format=True, errors='coerce')

    df['number-records'] = df['number-records'].round().astype('Int32')

    # Convert hyphens in keys to underscores.
    keys = list(df.keys())
    remove_underscores_map = {}
    for key in keys:
        remove_underscores_map[key] = key.replace('-', '_')
    df = df.rename(columns=remove_underscores_map)

    df.to_csv(csv_filename, index=False)

    # Replace any Nan or empty string in the supplier_legal_name with the
    # supplier_operating_name.
    df['supplier_legal_name'] = df['supplier_legal_name'].replace(
        np.nan, '', regex=True)
    df['supplier_legal_name'] = np.where(
        df['supplier_legal_name'] == '',
        df['supplier_operating_name'],
        df['supplier_legal_name'])

    unique_columns = [
        'contract_number',
        'amendment_number'
    ]
    export_for_postgres(df, postgres_csv_filename, unique_columns)

    unique_suppliers = list(df['supplier_legal_name'].fillna(value='').unique())
    unique_suppliers_norm = []
    for supplier in unique_suppliers:
        supplier = supplier.lower().strip()
        unique_suppliers_norm.append(supplier)
    unique_suppliers_norm = sorted(list(set(unique_suppliers_norm)))
    with open(unique_suppliers_filename, 'w') as outfile:
        json.dump(unique_suppliers_norm, outfile, indent=1)


def process_award_notices(
    csv_filename='ca_buyandsell_award_filings_orig.csv',
    postgres_csv_filename='ca_buyandsell_award_filing.csv',
    unique_suppliers_filename='ca_buyandsell_award_unique_suppliers.json'):
    # Read in the CSV as a DataFrame
    df = pd.read_csv(AWARDS_URL)

    # Drop the redundant French rows (only keeping the English)
    df = df[df['language'] != AWARDS_FRENCH_TAG]

    # Drop the 'procurement_entity_name' column (it should equal 'Public Works
    # and Government Services Canada'), the 'language' column (it should equal
    # 'English' in all entries), and the 'access_terms_of_use' column.
    df = df.drop(columns=[
        'procurement_entity_name', 'language', 'access_terms_of_use'])

    # Convert relevant columns to datetime.
    date_columns = ['award_date', 'publication_date', 'amendment_date']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(
            df[date_column], format='%Y-%m-%d', errors='coerce')
  
    df.to_csv(csv_filename, index=False)

    unique_columns = [
        'reference_number',
        'solicitation_number',
        'contract_sequence_number',
        'contract_number'
    ]
    export_for_postgres(df, postgres_csv_filename, unique_columns)

    unique_suppliers = list(df['supplier_info'].fillna(value='').unique())
    unique_suppliers_norm = []
    for supplier in unique_suppliers:
        supplier = supplier.lower().strip()
        unique_suppliers_norm.append(supplier)
    unique_suppliers_norm = sorted(list(set(unique_suppliers_norm)))
    with open(unique_suppliers_filename, 'w') as outfile:
        json.dump(unique_suppliers_norm, outfile, indent=1)


# TODO(Jack Poulson): Remove comments.
#process_proactive_disclosures()
process_pwsgc_contracts()
#process_award_notices()
